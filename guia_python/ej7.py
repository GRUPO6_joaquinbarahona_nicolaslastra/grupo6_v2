l=1500

from decimal import*
getcontext().prec = l

n=0
valor=0
cont=0

for n in range (0,l):
    a = Decimal(2 * pow(-1,n) * pow(3,0.5-n))
    b = Decimal(2 * n + 1)
    aux=a/b
    valor+=aux
    cont+=1

print("El valor de pi con",cont,"digitos es igual a",valor)

