#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//7EB3A50110140F3955025607EB7A50110140F39577301887EB3A50110140F3955025607EB7A50110140F3957730188

int main () {

    char *lectura, *lec1, *lec2, *lecT, *lecH, *tok;
    int n, n2, n3, data;

    lectura = malloc (200 * sizeof(char));
    strcpy (lectura, "7EB3A50110140F3955025607EB7A50110140F39577301887EB3A50110140F3955025607EB7A50110140F3957730188");
    n2 = strlen (lectura);
    lectura = realloc (lectura, n2 * sizeof(char));

    lecT = malloc (11 * sizeof(char));
    lecH = malloc (11 * sizeof(char));

    strcpy (lecT, "7EB3A501101"); //identificador lectura de temperatura
    strcpy (lecH, "7EB7A501101"); //identificador lectura de humedad

    tok = strtok (lectura, "4");
    n3 = strlen (tok);
    lec1 = malloc (n3 * sizeof(char));
    strcpy (lec1, tok);

    while (lec1 != NULL) {

        if (strcmp (lec1, lecT) == 0 || strcmp (lec1, "87EB3A501101") == 0) { //medicion temperatura
            tok = strtok (NULL, "5");
            tok = strtok (NULL, "0");
            tok = strtok (NULL, "0");
            n = strlen (tok);
            lec2 = malloc (n * sizeof(char));
            strcpy (lec2, tok);
            lec2 [2] = '\0'; //elimina caracter parte del checksum
            data = atoi (lec2);
            printf ("Data temperatura = %d\n", data);
        }

        if (strcmp (lec1, lecH) == 0 || strcmp (lec1, "87EB7A501101") == 0) { //medicion humedad
            tok = strtok (NULL, "7");
            tok = strtok (NULL, "8");
            n = strlen (tok);
            lec2 = malloc (n * sizeof(char));
            strcpy (lec2, tok);
            lec2 [0] = '0'; //elimina caracter parte de la direccion
            data = atoi (lec2);
            printf ("Data humedad = %d\n", data);
        }

        if (n2 == 24 || n2 == 23) //cuando solo se ingresa una lectura
           break;

        if (strcmp (lec1, "8") == 0) //cuando el final es de humedad
           break;

        tok = strtok (NULL, "4");
        n = strlen (tok);
        lec1 = realloc (lec1, n * sizeof(char));
        strcpy (lec1, tok);

    }//while

    free (lec1);
    free (lec2);
    free (lecT);
    free (lecH);
    return 0;
}
