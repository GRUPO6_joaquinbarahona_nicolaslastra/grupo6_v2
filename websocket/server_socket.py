import socket
import threading

HEADER = 64
PORT = 80
SERVER = '192.168.1.8'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'

direccion_mac = '45261404775600'

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind(ADDR)

def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")

    connected = True
    handshake = True
    while connected:
        hand1 = True
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)

        if handshake == True:
            if direccion_mac == msg:
                print(f"[Aceptado] Conexion aceptada {addr}")
                conn.send("aprobado".encode(FORMAT))
                handshake = False
                hand1 = False
            else:
                print(f"[Rechazado] Conexion rechazada {addr}")
                conn.send("[Rechazado] Conexion rechazada".encode(FORMAT))
                connected = False
                hand1 = False
        if msg == DISCONNECT_MESSAGE:
            conn.send("Desconectando...".encode(FORMAT))
            print(f"[{addr}] {msg}")
            print(f"[OFF] Server ha dejado de escuchar la direccion {addr}")
            connected = False
        elif msg != DISCONNECT_MESSAGE:
            if hand1 == True:
                print(f"[{addr}] {msg}")
                conn.send("Mensaje recibido".encode(FORMAT))
    conn.close()

def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")

    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")
print("[STARTING] server is running.....")

start()

