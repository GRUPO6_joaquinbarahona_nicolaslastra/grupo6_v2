import socket
import sys
from uuid import getnode 


HEADER = 64
PORT = 80
SERVER = '192.168.1.8'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'

MAC_str = str(getnode())
#MAC_str = str(MAC)

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)

def send(msg):
    message = msg.encode(FORMAT)

    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)

    send_length += b' '*(HEADER-len(send_length))

    client.send(send_length)
    client.send(message)
    mensaje = client.recv(2048).decode(FORMAT)

    if mensaje == "aprobado":
        print("[Aceptado] Conexion aceptada")
        return "aprobado"
    else:
        print(mensaje)

def enviar_info():
    input()
    send('mensaje 1')
    input()
    send('mensaje 2')
    input()
    send('mensaje 3')
    input()
    send(DISCONNECT_MESSAGE)
        
handshake = send(MAC_str)

if handshake == "aprobado":
    pass
else:
    sys.exit()

enviar_info()




