#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

void *binRandom( ) {
    //obtencion de numeros aleatorios
    static int r[256]; //matriz con numeros aleatorios
    int i;

    srand( (unsigned)time( NULL) );

    for(i = 0; i < 256; i++) {
        r[i] = rand () %256; //los numeros aleatorios estan entre 0 y 255
        printf("r[%d] = %d \n", i, r[i]);
    }

    //conversion a numero binario
    int x, j = 0, jaux;
    int bina [1024]; //almacena resto de la division por 2

    //recorre todos los numeros aleatorios de la matriz
    for (x = 0; x < 256; x++){
        while (r [x] != 0)  {
            bina [j] = r [x] % 2; //guarda el valor del resto
            r [x] = r [x] / 2; //divide por 2 (valores enteros)
            j++;
        }

        j--; //se ubica en la ultima posicion
        jaux = j; //variable auxiliar ara leds
        printf ("r[%d] = ", x);

        //recorre la matriz de restos para expresar el valor en binario
        while (j >= 0) {
            printf ("%d", bina [j]); //imprime desde el final al incio
            j--;
        }

        //recorre la matriz de restos para encender o apagar leds
        int y = 1;
        while (jaux >= 0) {
            if (bina [jaux] == 1) {
                printf (", Led %d ON", y);
            } else if (bina [jaux] == 0) {
                printf (",  Led %d OFF", y);
            }
            jaux--;
            y++;
        }

        printf ("\n");
        j = 0; //reincia la variable para el siguiente valor a convertir
        y = 0; //reincia la variable contadora de leds

    } //for que recorre la matriz de numeros aleatorios

    return 0;
} // binRandom


int main () {
    binRandom();
    return 0;
}

