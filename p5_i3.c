#include <stdio.h>
#include <math.h>

float media (float *arr, int n) {

    int i;
    double avg, sum = 0;

    for (i = 0; i < n; i++)
        sum += arr[i];

    avg = sum / n;
    return avg;
} //float media

float varianza (float *arr, int n, float prom) {

    int i;
    float var, resta, cuad, sum = 0;

    for (i = 0; i < n ; i++) {
        resta = arr [i] - prom;
        cuad = pow (resta, 2);
        sum += cuad;
    }

    var = sum / (n - 1);

    return var;
} //float var

float moda (float *arr, int n) {

    int i, j, cont, contaux, nmoda;
    float valormoda;

    contaux = cont;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if (arr [i] == arr [j] && i != j) {
                cont++;
            }
        }

        if (cont >= contaux) {
            cont = contaux;
            nmoda = i; //guarda la posicion del numero moda
        }
    }
    valormoda = arr [nmoda];
    return valormoda;
} //float moda

int main () {

    int n, i;
    float med, var, des, mod, dato;

    printf ("numero de elemetos = ");
    scanf ("%d", &n);

    float notas [n];

    for (i = 0; i < n; i++) {
        scanf ("%f", &dato);
        notas [i] = dato;
    }

    med = media (notas, n);
    var = varianza (notas, n, med);
    des = sqrt (var);
    mod = moda (notas, n);

    printf ("Media aritmetica = %.3f \n", med);
    printf ("Varianza = %.3f \n", var);
    printf ("Desviacion estandar = %.3f \n", des);
    printf ("Moda = %.3f \n", mod);

    return 0;
} // int main
