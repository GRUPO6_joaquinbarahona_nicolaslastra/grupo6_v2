#include <stdio.h>

float calc (float sonido) {
    float fa, fc;
    fa = sonido / 4 + 20;
    fc = (fa - 32) * 0.5555;
    return fc;
}


int main () {

    float fc, s;

    //lectura de la variable S
    printf ("s = ");
    scanf ("%f", &s);

    //llamada a la funcion
    fc = calc (s);
    printf ("Temperatura en Celsius = %.3f \n", fc);
    return 0;
} //int main

