#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int * getRandom () {

    static int r[24];
    int i;

    srand( (unsigned)time( NULL) );

    for ( i = 0; i < 24; i++ ) {
        r[i] = rand ();
        printf( "r[%d] = %d \n", i+1 , r[i] );
    }

    return r;
}

double getAverage (int *arr) {

    int i;
    double avg, sum = 0;

    for (i = 0; i < 24; i++)
        sum += arr[i];

    avg = sum / 24;
    return avg;
}

void maxmin (int *rnd) {
    int i, maxtemp [2], mintemp [2];

    maxtemp [0] = rnd [0];
    mintemp [0] = rnd [0];

    for ( i = 1; i < 24; i++ ) {
        if (maxtemp [0] < rnd [i]) {
            maxtemp [0] = rnd [i]; //temp maxima
            maxtemp [1] = i + 1; //hora maxima
        }

        if (mintemp [0] > rnd [i]) {
            mintemp [0] = rnd [i]; //temp minima
            mintemp [1] = i + 1; //hora minimo
        }
    }

    printf ("Temperatura maxima = %d ", maxtemp [0]);
    printf (", en la hora = %d \n", maxtemp [1]);
    printf ("Temperatura mimina = %d ", mintemp [0]);
    printf (", en la hora = %d \n", mintemp [1]);
    return;
}

int main () {

    int *TEM;
    double avg;

    TEM = getRandom ();
    avg = getAverage (TEM);
    maxmin (TEM);
    printf ("Temperatura promedio = %.3lf \n", avg);

    return 0;
}

