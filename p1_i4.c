#include <stdio.h>
#include <string.h>

int main (){
    char cad [50];
    char *cadena;

    printf ("Ingrese una cadena de maximo 50 caracteres:\n");
    scanf ("%s",cad);
    cadena=cad;

    int i, n, carac;
    int cont;

    n = strlen (cadena);
     //printf ("Valor de n: %d\n", n);

    char car[1];
    char *lyric;
    int letra;

    printf ("Ingrese caracter a buscar:\n");
    scanf ("%s",car);
    lyric=car;
    letra=lyric[0];
    //printf ("Imprimiendo lo que sale de letra: %d\n", letra);

    for (i=0; i<n; i++){
        carac=cadena[i];
        //printf ("Imprimiendo lo que sale de carac: %d\n", carac);
        if(carac == letra){
            cont++;
        }
    }
    printf ("El caracter '%s' esta: %d veces\n", car, cont);

    return 0;
}
