#include <stdio.h>
#include <string.h>

int transf (int carac) {
    int tran;
    if (carac == 49)
        tran = 1;
    else if (carac == 50)
        tran = 2;
    else if (carac == 51)
        tran = 3;
    else if (carac == 52)
        tran = 4;
    else if (carac == 53)
        tran = 5;
    else if (carac == 54)
        tran = 6;
    else if (carac == 55)
        tran = 7;
    else if (carac == 56)
        tran = 8;
    else
        tran = 9;

    return tran;
}

void lector (char *cadena) {
    int i, j, n, carac;
    n = strlen (cadena);
    for (i = 0; i < n ; i+=2){
        carac = transf (cadena [i]);
        for (j = 0; j < carac; j++){
            printf ("%c", cadena [i+1]);
        }
    }
    printf ("\n");
    return;
}

int main () {
    char cad [50];
    char *cadena;

    printf ("cadena de maximo 50 caracteres: ");
    scanf ("%s", cad);
    cadena = cad;
    lector (cadena);
    return 0;
}











